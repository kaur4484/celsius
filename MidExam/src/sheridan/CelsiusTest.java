package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIn() {
		assertTrue("can not convert to Celsius", Celsius.convertFromFahrenheit(20)==52);
		
	}


	@Test
	public void testOut() {
		assertFalse("Not a valid input",Celsius.convertFromFahrenheit(20)==33);
		
	}
	

	@Test
	public void testBoundIn() {
		assertTrue("can not convert to Celsius",Celsius.convertFromFahrenheit(20)==33);
		
	}
	

	@Test
	public void testBoundOut() {
		assertFalse("Logical error",Celsius.convertFromFahrenheit(20)==33);
		
	}
	
}